import pages.*;
import shop.Shop;
import utils.*;

public class Main {
    private static AbstractPage currentPage;

    public static void main(String[] args) {
        UsersManager usersManager = new UsersManager();
        Shop shop = new Shop();

        ProductsListPage productsListPage = new ProductsListPage("Show products list", shop);

        AbstractPage adminMainPage = new MainPage(new AbstractInfoPage[] {productsListPage});
        AbstractPage userMainPage = new MainPage(new AbstractInfoPage[] {productsListPage});

        AbstractPage authPage = new AuthorizePage(usersManager, adminMainPage, userMainPage);

        currentPage = authPage;

        while (true) {
            AbstractPage nextPage = currentPage.process();

            if (nextPage == null)
                break;

            currentPage = nextPage;
        }
    }
}