package pages;

public abstract class AbstractPage {

    public AbstractPage process() {
        System.out.println("-------------------");

        AbstractPage nextPage = processPage();

        System.out.println("-------------------");

        return nextPage;
    }

    protected abstract AbstractPage processPage();
}