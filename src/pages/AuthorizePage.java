package pages;

import utils.UserRole;
import utils.UsersManager;

import java.util.Scanner;

public class AuthorizePage extends AbstractPage {
    private UsersManager usersManager;
    private Scanner in;
    private AbstractPage adminPage;
    private AbstractPage userPage;

    public AuthorizePage(UsersManager usersManager, AbstractPage adminPage, AbstractPage userPage) {
        this.usersManager = usersManager;
        this.adminPage = adminPage;
        this.userPage = userPage;

        in = new Scanner(System.in);
    }

    @Override
    protected AbstractPage processPage() {
        System.out.println("Welcome to shop!");

        while (true) {
            System.out.println("Enter your login:");
            String name = in.nextLine();
            System.out.println("Enter your password:");
            String password = in.nextLine();

            if (usersManager.tryAuthorise(name, password)) {
                System.out.println("Welcome, " + name + "!");
                break;
            }

            System.out.println("Wrong login or password, try again");
            System.out.println();
        }

        if (usersManager.getAuthorizedUser().role == UserRole.ADMIN)
            return adminPage;

        if (usersManager.getAuthorizedUser().role == UserRole.USER)
            return userPage;

        return null;
    }
}