package pages;

import java.util.Scanner;

public class MainPage extends AbstractPage {
    private AbstractInfoPage[] pages;

    private Scanner in;

    public MainPage(AbstractInfoPage[] pages) {
        this.pages = pages;

        in = new Scanner(System.in);
    }

    @Override
    protected AbstractPage processPage() {
        if (pages == null || pages.length == 0) {
            System.out.println("There's no options");
            return null;
        }

        System.out.println("Choose your option: ");
        printPagesInfo();

        int option = in.nextInt();

        if (option >= 0 && option < pages.length)
            return pages[option];

        return null;
    }


    private void printPagesInfo() {
        for (int i = 0; i < pages.length; i++) {
            System.out.println(i + ": " + pages[i].name);
        }
    }
}
