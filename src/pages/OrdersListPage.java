package pages;

import shop.Shop;
import utils.User;
import utils.UserRole;
import utils.UsersManager;

public class OrdersListPage extends AbstractInfoPage {
    private Shop shop;
    private UsersManager usersManager;

    public OrdersListPage(String name, Shop shop, UsersManager usersManager) {
        this.name = name;
        this.shop = shop;
        this.usersManager = usersManager;
    }

    @Override
    protected AbstractPage processPage() {
        User user = usersManager.getAuthorizedUser();

        if (user.role == UserRole.ADMIN)
            printAllOrders();
        else
            printUserOrders(user);

        return null;
    }

    private void printAllOrders() {
        System.out.println("All orders in shop:");
        shop.printOrders();
    }

    private void printUserOrders(User user) {
        System.out.println("Orders for user: " + user.name);
        shop.printOrdersByUserName(user.name);
    }
}