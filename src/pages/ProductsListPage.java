package pages;

import shop.Shop;

public class ProductsListPage extends AbstractInfoPage {
    private Shop shop;

    public ProductsListPage(String name, Shop shop) {
        this.name = name;
        this.shop = shop;
    }

    @Override
    protected AbstractPage processPage() {
        System.out.println("List of products:");
        shop.printProducts();

        return null;
    }
}