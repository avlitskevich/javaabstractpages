package shop;

import utils.User;

public class Order {
    private User user;
    private Product product;

    public Order(User user, Product product) {
        this.user = user;
        this.product = product;
    }

    boolean checkUserName(String name) { return user.name.equals(name); }

    @Override
    public String toString() { return user.name + " - " + product.name; }
}