package shop;

import java.util.ArrayList;

public class Shop {
    private ArrayList<Product> products;
    private ArrayList<Order> orders;

    public Shop() {
        createDefaultProducts();
        orders = new ArrayList<>();
    }

    public void printProducts() {
        for (int i = 0; i < products.size(); i++)
            System.out.println(i + ": " + products.get(i).toString());
    }

    public void printOrders() {
        for (int i = 0; i < orders.size(); i++) {
            System.out.println(i + ": " + orders.get(i).toString());
        }
    }

    public void printOrdersByUserName(String name) {
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).checkUserName(name))
                System.out.println(i + ": " + orders.get(i).toString());
        }
    }

    public void addProduct(Product product) { products.add(product); }

    public void addOrder(Order order) { orders.add(order); }

    public Product getProductById(int id) { return products.get(id); }


    private void createDefaultProducts() {
        products = new ArrayList<>();
        products.add(new Product("Phone", 10));
        products.add(new Product("Car", 1000));
        products.add(new Product("Table", 20));
    }
}