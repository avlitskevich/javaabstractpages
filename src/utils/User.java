package utils;

public class User {
    public UserRole role;
    public String name;

    String password;

    public User(String name, String password, UserRole role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }
}