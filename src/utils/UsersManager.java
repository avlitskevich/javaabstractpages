package utils;

import java.util.ArrayList;

public class UsersManager {
    private ArrayList<User> users;

    private User authorizedUser;

    public UsersManager() {
        createDefaultUsers();
    }

    public boolean tryAuthorise(String name, String password) {
        for (User user : users) {
            if (user.name.equals(name) && user.password.equals(password)) {
                authorizedUser = user;
                return true;
            }
        }

        return false;
    }

    public User getAuthorizedUser() { return authorizedUser; }


    private void createDefaultUsers() {
        users = new ArrayList<>();
        users.add(new User("admin", "admin", UserRole.ADMIN));
        users.add(new User("user", "user", UserRole.USER));
    }
}